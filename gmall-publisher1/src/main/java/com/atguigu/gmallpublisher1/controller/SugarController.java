package com.atguigu.gmallpublisher1.controller;

import com.atguigu.gmallpublisher1.service.ProductStatsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

//@Controller
@RestController
@RequestMapping("/api/sugar")
public class SugarController {

    @Autowired
    ProductStatsService productStatsService;

    @RequestMapping("/test")
    public String test1() {
        System.out.println("aaaaaaaaaaa");
//        return "success";
        return "index.html";
    }

    @RequestMapping("/test2")
    //@ResponseBody
    public String test2() {
        System.out.println("aaaaaaaaaaa");
        return "success";
    }

    @RequestMapping("/test3")
    public String test3(@RequestParam("name") String nn,
                        @RequestParam(value = "age", defaultValue = "18") int age) {
        System.out.println(nn + ":" + age);
        return "success";
    }

    @RequestMapping("/gmv")
    public String getGmv(@RequestParam(value = "date", defaultValue = "0") int date) {

        if (date == 0) {
            date = getToday();
        }

        BigDecimal gmv = productStatsService.getGmv(date);

        return "{ " +
                "  \"status\": 0, " +
                "  \"msg\": \"\", " +
                "  \"data\": " + gmv +
                "}";
    }

    @RequestMapping("/tm")
    public String getGmvByTm(@RequestParam(value = "date", defaultValue = "0") int date,
                             @RequestParam(value = "limit", defaultValue = "5") int limit) {

        if (date == 0) {
            date = getToday();
        }

        //查询ClickHouse获取数据
        Map gmvByTm = productStatsService.getGmvByTm(date, limit);

        //获取品牌名称以及对应的销售额
        Set tmName = gmvByTm.keySet();
        Collection orderAmount = gmvByTm.values();

        //封装JSON数据并返回
        return "{ " +
                "  \"status\": 0, " +
                "  \"msg\": \"\", " +
                "  \"data\": { " +
                "    \"categories\": [\"" +
                StringUtils.join(tmName, "\",\"") +
                "\"], " +
                "    \"series\": [" +
                "      { " +
                "        \"name\": \"GMV\", " +
                "        \"data\": [" +
                StringUtils.join(orderAmount, ",") +
                "] " +
                "      } " +
                "    ] " +
                "  } " +
                "}";

    }

    private int getToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        long ts = System.currentTimeMillis();
        return Integer.parseInt(sdf.format(ts));
    }

}
