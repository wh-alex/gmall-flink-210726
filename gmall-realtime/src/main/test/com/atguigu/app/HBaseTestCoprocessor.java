package com.atguigu.app;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HBaseTestCoprocessor {

    public static void main(String[] args) throws IOException {

        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", "hadoop102,hadoop103,hadoop104");
        Connection connection = ConnectionFactory.createConnection(configuration);

        Admin admin = connection.getAdmin();

        TableDescriptor tableDescriptor = TableDescriptorBuilder.newBuilder(TableName.valueOf("aa"))
                .setColumnFamily(ColumnFamilyDescriptorBuilder.of("info"))
                .setCoprocessor("com.atguigu.bean.MyCoprocessor")
                .build();

        admin.createTable(tableDescriptor);

        admin.close();
        connection.close();
    }

}
