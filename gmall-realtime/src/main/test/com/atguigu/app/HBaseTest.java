package com.atguigu.app;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HBaseTest {

    public static void main(String[] args) throws IOException {

        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", "hadoop102,hadoop103,hadoop104");
        Connection connection = ConnectionFactory.createConnection(configuration);

        //DDL
        Admin admin = connection.getAdmin();
        HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf("aaaa"));
        //tableDescriptor.addCoprocessor()
        admin.createTable(tableDescriptor);

        //DML
        Table table = connection.getTable(TableName.valueOf("test210726"));

        Delete delete = new Delete("1006".getBytes());
        //delete.addFamily("info1".getBytes());
        //delete.addColumn();
        delete.addColumns("info1".getBytes(), "name".getBytes());

        Scan scan = new Scan();
        //scan.setFilter()
        ResultScanner tableScanner = table.getScanner(scan);

        table.delete(delete);

        table.close();
        connection.close();


    }

}
